/* header files */
#include "fileutils.h"
#include "raag.h" //for RAAG_FOLDER_PATH
#include<unistd.h>

/* rs_list_dir: lists the directory contents of the directory
 * here it is used for listing the list of raagas available in a folder */
void rs_list_dir(char *name)
{
	//local variables
	DIR *dir; //directory pointer; returned by opendir
	struct dirent *direntry; //for contents of the file; returned by readdir
	int i=0; //for sr. no.

	//code
	if((dir = opendir(name))==NULL) //opens the directory
	{
		//quit("opendir", 1);
		printf("directory %s not present. Creating it now.\n",name);
		mkdir(name, S_IRUSR | S_IWUSR | S_IXUSR
			| S_IRGRP | S_IROTH);

		//open this directory now
		if((dir = opendir(name))==NULL)
		{
			quit("opendir", 1);
		}
	}

	//iterate through every file from the directory
	while((direntry = readdir(dir)) != NULL)
	{
		printf("%5d. %s\n", (++i), direntry->d_name);
	}

	//close the directory
	closedir(dir);
	//exit(0);
}


/* display file contents in the raag directory */
void rs_view_file_contents(int fileno, int isViewExportedFile, int isExportToFile)
{
	//local variables
	DIR *dir; //directory pointer; returned by opendir
	struct dirent *direntry; //for contents of the file; returned by readdir
	int i=0; //for sr. no.
	char *lfile_path;
	char raag_file_name[50];
	char *lexport_path;
	struct raag *exported; //raag to be exported

	//code
	if(isViewExportedFile == 0) //if not to view exported file
	{
	if((dir = opendir(RAAG_DATA_FOLDER_PATH))==NULL) //opens the directory
	{
		printf("directory not present\n");
		quit("opendir", 1);
	}
	//iterate through every file from the directory
	while((direntry = readdir(dir)) != NULL)
	{
		i++;
		if(i==fileno) //your file
		{
			//allocate size equal to fully qualified name of raag file
			lfile_path = (char *) rs_alloc(rs_strlen(RAAG_DATA_FOLDER_PATH) + rs_strlen(direntry->d_name) + 1);

			//create relative path of raagas folder	
			rs_strstr(lfile_path, RAAG_DATA_FOLDER_PATH,
					direntry->d_name);



			//import raag
			exported = malloc(sizeof(struct raag));
			import_raag(lfile_path, exported);
			//rs_show_file_contents(lfile_path); //show the contents on the disk


			//check if file needs to be exported
			if(isExportToFile == 1)
			{
				//takes the input from the user
				printf("Enter the name of the file:\n");
				rs_getline(raag_file_name); //get file name
				lexport_path = malloc(rs_strlen(RAAG_FOLDER_PATH) + rs_strlen(raag_file_name) + rs_strlen(RAAG_FILE_EXT) + 1);
				//final export path
				rs_create_file_path(lexport_path, RAAG_FOLDER_PATH, raag_file_name, RAAG_FILE_EXT);
				//exporting to file
				printf("Exporting to file - %s\n",lexport_path);
				writeToFile(exported, lexport_path);
				
				free(lexport_path); //free memory of raag file name
			}else
			{
				//prints raag to the console
				printRaag(exported);
			}
			free(exported); //free the memory of raag
			break;
		}
	}
	}//end of outermost if
	else if(isViewExportedFile == 2) //update action
	{
		if((dir = opendir(RAAG_DATA_FOLDER_PATH))==NULL) //opens the directory
	{
		printf("directory not present\n");
		quit("opendir", 1);
	}
	//iterate through every file from the directory
	while((direntry = readdir(dir)) != NULL)
	{
		i++;
		if(i==fileno) //your file
		{
			//allocate size equal to fully qualified name of raag file
			lfile_path = (char *) rs_alloc(rs_strlen(RAAG_DATA_FOLDER_PATH) + rs_strlen(direntry->d_name) + 1);

			//create relative path of raagas folder	
			rs_strstr(lfile_path, RAAG_DATA_FOLDER_PATH,
					direntry->d_name);



			//import raag
			exported = malloc(sizeof(struct raag));
			import_raag(lfile_path, exported);
			//rs_show_file_contents(lfile_path); //show the contents on the disk

			//choose which data to update
			printf("1. Raag Name\n");
			printf("2. Thaat\n");
			printf("3. Warjya Swar\n");
			printf("4. Jati\n");
			printf("5. Wadi Swar\n");
			printf("6. Samvadi Swar\n");
			printf("7. Swar Samay\n");
			printf("8. Description\n");
			printf("9. Aaroh\n");
			printf("10. Avroh\n");
			printf("11. Pakad\n");
			printf("12. Swar Vistar\n");
			printf("13. Sargam - Sthai\n");
			printf("14. Sargam - Antara\n");
			printf("15. Gat - Sthai\n");
			printf("16. Gat - Antara\n");
			printf("17. Aalap - Sthai\n");
			printf("18. Aalap - Antara\n");
			printf("19. Taana - Sthai\n");
			printf("20. Taana - Antara\n");
			printf("Choose which data you want to update in raag %s\n",exported->name);
			char in[2];
			char last_token[512];
			//char input_text[512];
			rs_getline(in);
			struct raag *r = exported;
			printf("Enter details:\n");
			rs_getlines(last_token);

			//convert to int
			int fieldNo = in[0] - '0';
			printf("field - %d\n", fieldNo);
			switch(fieldNo)
			{
				case 1:
				r->name = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->name, last_token); //copy buffer contents to struct
				
				break;
				case 2: //thaat
				r->thaat = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->thaat, last_token); //copy buffer contents to struct
				break;
				case 3: //warjya swar
				r->warjya_swar = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->warjya_swar, last_token); //copy buffer contents to struct
				break;
				case 4://jati
				r->jati = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->jati, last_token); //copy buffer contents to struct
				break;
				case 5: //vadi swar
				r->vadi_swar = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->vadi_swar, last_token); //copy buffer contents to struct
				break;
				case 6: //samvadi swar
				r->samvadi_swar = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->samvadi_swar, last_token); //copy buffer contents to struct
				break;
				case 7: //swar samay
				r->swar_samay = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->swar_samay, last_token); //copy buffer contents to struct
				break;
				case 8: //description
				r->raag_desc = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->raag_desc, last_token); //copy buffer contents to struct
				break;
				case 9: //aaroh
				r->intro.aaroh = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->intro.aaroh, last_token); //copy buffer contents to struct
				break;
				case 10: //avroh
				r->intro.avroh = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->intro.avroh, last_token); //copy buffer contents to struct
				break;
				case 11: //pakad
				r->intro.pakad = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name

					rs_strcpy(r->intro.pakad, last_token); //copy buffer contents to struct
				break;
				case 12: //swar vistar
				r->intro.swar_vistar = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->intro.swar_vistar, last_token); //copy buffer contents to struct
				break;
				case 13: //sargam - sthai
				r->sargam.sthai = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->sargam.sthai, last_token); //copy buffer contents to struct
				break;
				case 14: //sargam - antara
				r->sargam.antara = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->sargam.antara, last_token); //copy buffer contents to struct
				break;
				case 15: //gat - sthai
				r->gat.sthai = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->gat.sthai, last_token); //copy buffer contents to struct
				break;
				case 16: //gat - antara
				r->gat.antara = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->gat.antara, last_token); //copy buffer contents to struct
				break;

				case 17: //aalap - sthai
				r->aalap.sthai = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->aalap.sthai, last_token); //copy buffer contents to struct
				break;
				case 18: //aalap - antara
				r->aalap.antara = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->aalap.antara, last_token); //copy buffer contents to struct
				break;

				case 19: //taana - sthai
				r->taana.sthai = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->taana.sthai, last_token); //copy buffer contents to struct
				break;
				case 20: //taana - antara
				if(last_token != NULL)
				{
				r->taana.antara = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->taana.antara, last_token); //copy buffer contents to struct
				}
				break;

			}//end of switch

			//write to file
			save(r, lfile_path);
			rs_free(r);
			break; //break if file match found
		}
	}	
	}
	else //if exported file
	{
		i=0; //reset i
		if((dir = opendir(RAAG_FOLDER_PATH))==NULL) //opens the directory
		{
			printf("directory not present\n");
			quit("opendir", 1);
		}
		//iterate through every file from the directory
		while((direntry = readdir(dir)) != NULL)
		{
			i++;
			if(i==fileno) //your file
			{
			//allocate size equal to fully qualified name of raag file
			lfile_path = (char *) rs_alloc(rs_strlen(RAAG_FOLDER_PATH) + rs_strlen(direntry->d_name) + 1);

			//create relative path of raagas folder	
			rs_strstr(lfile_path, RAAG_FOLDER_PATH,
					direntry->d_name);



			rs_show_file_contents(lfile_path); //show the contents on the disk
			break;
			}
		}
	} //end of else
	if(lfile_path != NULL)
	{
		//free the above memory
		rs_free(lfile_path);
	}
	//close the directory
	closedir(dir);

}

/* createFilePath: create file path and store it in result */
void rs_create_file_path(char *result, char *folder, char *filename, char *ext)
{
	//append filename to folder name and store it in result	
	rs_strstr(result, folder, filename);

	//append extension to the result and store it in result
	rs_strstr(result, result, ext);
}


/* rs_show_file_contents: read the file contents and show it on the screen */
int rs_show_file_contents(char *path)
{
	//local variables
	int fd;
	int c;
	int count=512;
	char buf[count];
	//code
	fd = open(path, O_RDONLY); //open file for reading
	//check any exception
	if(fd == -1)
	{
		quit("fopen", 1);
	}

	//read file contents
	while((c=read(fd, buf, count)) > 0)
	{
		write(STDOUT_FILENO, buf, c); //write to standard output
	}
	fprintf(stdout, "\n\nFile Read Successfully...\n");


	//close the fd
	close(fd);
}
