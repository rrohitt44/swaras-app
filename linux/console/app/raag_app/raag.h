#ifndef H_RAAG
#define H_RAAG
/* header files */
#include<stdio.h>

/* global variables / macros */
#define MAXBUF 1000 //max buffer size
#define RAAG_FOLDER_LENGTH 50 //max length of relative path of raagas folder
#define RAAG_FILE_EXT_LENGTH 10 //max length of the file extension of the raag file

char buffer[MAXBUF]; //buffer to store input
extern char RAAG_DATA_FOLDER_PATH[RAAG_FOLDER_LENGTH];
extern char RAAG_DATA_FILE_EXT[RAAG_FILE_EXT_LENGTH];
extern char RAAG_FOLDER_PATH[RAAG_FOLDER_LENGTH];
extern char RAAG_FILE_EXT[RAAG_FILE_EXT_LENGTH];
extern char *file_path;

struct raag_intro //introduction to the raag
{
	char *aaroh;
	char *avroh;
	char *pakad;
	char *swar_vistar;
};

struct raag_sthayi_antara //for sargam, gat, aalap, tana
{
	char *sthai;
	char *antara;
};

struct raag //all the information about the raag
{
	char *name;
	char *thaat;
	char *warjya_swar;
	char *jati;
	char *vadi_swar;
	char *samvadi_swar;
	char *swar_samay;
	char *raag_desc;
	
	struct raag_intro intro;
	struct raag_sthayi_antara sargam;
	struct raag_sthayi_antara gat;
	struct raag_sthayi_antara aalap;
	struct raag_sthayi_antara taana;
};


/*function prototype declarations */
struct raag *getRaagData(void);
void printRaag(struct raag *r);
void save(struct raag *r, char *path); //save the raag as a data
void writeToFile(struct raag *r, char *name);
void import_raag(char *path, struct raag *r); //import raag into struct r
void export_raag(FILE *file, struct raag *r); //export raag as per the fd provided
#endif
