/* header files */
#include "quit.h"

/* quit: prints the error message and exit the program */
void quit(char *message, int status)
{
	perror(message);
	exit(status);
}
