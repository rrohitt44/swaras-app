/* header files */
#include "console.h"

/* rs_getline: takes input from standard input and store it in buffer and returns the number of characters read */
int rs_getline(char *buffer)
{
	int i=0; //while loop index
	int c = getchar(); //get the character from standard input
	while(c!=EOF && c!='\n') //loop till EOF && newline not found
	{
		buffer[i] = c; //store character in buffer
		c = getchar(); //get next character from standard input
		i++; //increament loop counter
	}
	buffer[i]='\0'; //end the buffer with null terminated character
	return i;
}

/* rs_getlines: takes multiple lines as an input from standard input and store it in buffer and returns the number of characters read */
int rs_getlines(char *buffer)
{
	int i=0; //while loop index
	int c = getchar(); //get the character from standard input
	while(c!=EOF) //loop till EOF
	{
		buffer[i] = c; //store character in buffer
		c = getchar(); //get next character from standard input
		i++; //increament loop counter
	}
	//i=i-1; //to ignore last newline character
	buffer[i]='\0'; //end the buffer with null terminated character
	return i;
}

/* getch: get one character as a input from console*/
int getch(void)
{
	int ch;
	ch=getchar();
	while(getchar()!='\n');
	return ch;
}
