#ifndef H_CONSOLE
#define H_CONSOLE
/* header files */
#include<stdio.h>

/* function prototype declarations */
int rs_getline(char *buffer); //gets the line input from console
int rs_getlines(char *buffer); //gets multiple lines as a input from console
int rs_getch(); //get one character from console
#endif
