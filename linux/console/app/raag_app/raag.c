/* header files */
#include "raag.h"
#include "quit.h"
#include "console.h"
#include "strutils.h"
#include "memutils.h"
#include "string.h"

/* global variables */
struct raag r;
char RAAG_DATA_FOLDER_PATH[] = "data/";
char RAAG_DATA_FILE_EXT[] = ".raag";
char RAAG_FOLDER_PATH[] = "raagas/";
char RAAG_FILE_EXT[] = ".txt";

/* getRaagData: takes the details of the raag from console */
struct raag *getRaagData(void)
{
	//local variables

	//get Raag name
	printf("Enter raag name\n");
	int len = rs_getline(buffer); //get input from standard input
	r.name = (char *) rs_alloc(rs_strlen(buffer)+1); //allocate size of source to name
	rs_strcpy(r.name, buffer); //copy buffer contents to struct
	printf("\n");

	//get Raag thaat
	printf("Enter thaat\n");
	len = rs_getline(buffer);
	r.thaat = (char *) rs_alloc(rs_strlen(buffer)+1); //allocate size of source to thaat
	rs_strcpy(r.thaat, buffer); //copy buffer to struct
	printf("\n");

	//get warjya swar
	printf("Enter warjya swar\n");
	len = rs_getline(buffer); //get input from standard input
	r.warjya_swar = (char *) rs_alloc(rs_strlen(buffer)+1); //allocate size of source to warjya swar
	rs_strcpy(r.warjya_swar, buffer); //copy buffer contents to struct
	printf("\n");

	//get jati
	printf("Enter jati\n");
	len = rs_getline(buffer); //get input from standard input
	r.jati = (char *) rs_alloc(rs_strlen(buffer)+1); //allocate size of source to jati
	rs_strcpy(r.jati, buffer); //copy buffer contents to struct
	printf("\n");

	//get vadi swar
	printf("Enter vadi swar\n");
	len = rs_getline(buffer); //get input from standard input
	r.vadi_swar = (char *) rs_alloc(rs_strlen(buffer)+1); //allocate size of source to vadi swar
	rs_strcpy(r.vadi_swar, buffer); //copy buffer contents to struct
	printf("\n");

	//get samwadi swar
	printf("Enter samwadi swar\n");
	len = rs_getline(buffer); //get input from standard input
	r.samvadi_swar = (char *) rs_alloc(rs_strlen(buffer)+1); //allocate size of source to samvadi swar
	rs_strcpy(r.samvadi_swar, buffer); //copy buffer contents to struct
	printf("\n");

	
	//get swar samay
	printf("Enter swar samay\n");
	len = rs_getline(buffer); //get input from standard input
	r.swar_samay = (char *) rs_alloc(rs_strlen(buffer)+1); //allocate size of source to samay
	rs_strcpy(r.swar_samay, buffer); //copy buffer contents to struct
	printf("\n");


	//get description
	printf("Enter description\n");
	len = rs_getline(buffer); //get input from standard input
	r.raag_desc = (char *) rs_alloc(rs_strlen(buffer)+1); //allocate size of source to desc
	rs_strcpy(r.raag_desc, buffer); //copy buffer contents to struct
	printf("\n");
	printf("\n");

	/* Get aaroh avroh pakad swar_vistar of raag */
	printf("\n-------Raag introduction------\n");	
	printf("Enter aaroh\n");
	len = rs_getline(buffer); //get input from standard input
	r.intro.aaroh = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.intro.aaroh, buffer); //copy buffer contents to struct
		
	printf("Enter avroh\n");
	len = rs_getline(buffer); //get input from standard input
	r.intro.avroh = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.intro.avroh, buffer); //copy buffer contents to struct

	printf("Enter pakad\n");
	len = rs_getlines(buffer); //get input from standard input
	r.intro.pakad = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.intro.pakad, buffer); //copy buffer contents to struct

	printf("Enter swar-vistar\n");
	len = rs_getlines(buffer); //get input from standard input
	r.intro.swar_vistar = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.intro.swar_vistar, buffer); //copy buffer contents to struct


	/* Get Sargam, Gat, Aalap, Taana */
	printf("\n-----Sargam Gat Aalap Taana-----\n");
	printf("\n-----Sargam-----\n");
	printf("Enter Sthai\n");
	len = rs_getlines(buffer); //get input from standard input
	r.sargam.sthai = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.sargam.sthai, buffer); //copy buffer contents to struct

	printf("Enter Antara\n");
	len = rs_getlines(buffer); //get input from standard input
	r.sargam.antara = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.sargam.antara, buffer); //copy buffer contents to struct

	printf("\n-----Gat-----\n");
	printf("Enter Sthai\n");
	len = rs_getlines(buffer); //get input from standard input
	r.gat.sthai = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.gat.sthai, buffer); //copy buffer contents to struct

	printf("Enter Antara\n");
	len = rs_getlines(buffer); //get input from standard input
	r.gat.antara = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.gat.antara, buffer); //copy buffer contents to struct

	printf("\n-----Aalap-----\n");
	printf("Enter Sthai\n");
	len = rs_getlines(buffer); //get input from standard input
	r.aalap.sthai = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.aalap.sthai, buffer); //copy buffer contents to struct

	printf("Enter Antara\n");
	len = rs_getlines(buffer); //get input from standard input
	r.aalap.antara = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.aalap.antara, buffer); //copy buffer contents to struct

	printf("\n-----Taana-----\n");
	printf("Enter Sthai\n");
	len = rs_getlines(buffer); //get input from standard input
	r.taana.sthai = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.taana.sthai, buffer); //copy buffer contents to struct

	printf("Enter Antara\n");
	len = rs_getlines(buffer); //get input from standard input
	r.taana.antara = (char *) rs_alloc(rs_strlen(buffer) + 1); //allocate size
	rs_strcpy(r.taana.antara, buffer); //copy buffer contents to struct



	return &r;
}

void printRaag(struct raag *r)
{
	printf("******************** RAAG %s **********************\n",r->name);
	printf("Name - %s\n",r->name);
	printf("Thaat - %s\n",r->thaat);
	printf("Warjya swar - %s\n",r->warjya_swar);
	printf("Jati - %s\n",r->jati);
	printf("Vadi swar - %s\n",r->vadi_swar);
	printf("Samvadi swar - %s\n",r->samvadi_swar);
	printf("Swar samay - %s\n",r->swar_samay);
	printf("Raag description\n");
	printf("-----------------\n");
	printf(" %s\n",r->raag_desc);
	printf("-------------------------------------------------------------------\n");
	printf("\n------Raag Introduction-----\n");
	printf("Aaroh\n");
	printf("-----\n");
	printf("%s\n\n",r->intro.aaroh);
	printf("Avroh\n");
	printf("------\n");
	printf("%s\n\n",r->intro.avroh);
	printf("Pakad\n");
	printf("------\n");
	printf("%s\n\n",r->intro.pakad);
	printf("Swar-vistar\n");
	printf("------------\n");
	printf("%s\n\n",r->intro.swar_vistar);

	printf("-------------------------------------------------------------------\n");
	printf("\n\n-----Sargam-----\n");
	printf("Sthai\n");
	printf("------\n");
	printf("%s\n",r->sargam.sthai);
	printf("Antara\n");
	printf("-------\n");
	printf("%s\n",r->sargam.antara);

	printf("-------------------------------------------------------------------\n");
	printf("\n\n-----Gat-----\n");
	printf("Sthai\n");
	printf("------\n");
	printf("%s\n",r->gat.sthai);
	printf("Antara\n");
	printf("-------\n");
	printf("%s\n",r->gat.antara);

	printf("-------------------------------------------------------------------\n");
	printf("\n\n-----Aalap-----\n");
	printf("Sthai\n");
	printf("------\n");
	printf("%s\n",r->aalap.sthai);
	printf("Antara\n");
	printf("-------\n");
	printf("%s\n",r->aalap.antara);

	printf("-------------------------------------------------------------------\n");
	printf("\n\n-----Taana-----\n");
	printf("Sthai\n");
	printf("------\n");
	printf("%s\n",r->taana.sthai);
	printf("Antara\n");
	printf("-------\n");
	printf("%s\n",r->taana.antara);
	printf("-------------------------------------------------------------------\n");
	printf("***********************End**********************");

}

/* writeToFile: exports the raag to the file for end users */
void writeToFile(struct raag *r, char *name)
{
	//local variables
	FILE *file;
	//code
	/* Write contents to file */
	file = fopen(name, "w+"); //open the file in write mode, create if not present
	if(file == NULL)
	{
		printf("%s folder not found\n",name);
		quit("writeToFile", 1);
	}
	//write to the file
	fprintf(file,"******************** RAAG %s **********************\n",r->name);
	fprintf(file,"Name - %s\n",r->name);
	fprintf(file,"Thaat - %s\n",r->thaat);
	fprintf(file,"Warjya swar - %s\n",r->warjya_swar);
	fprintf(file,"Jati - %s\n",r->jati);
	fprintf(file,"Vadi swar - %s\n",r->vadi_swar);
	fprintf(file,"Samvadi swar - %s\n",r->samvadi_swar);
	fprintf(file,"Swar samay - %s\n",r->swar_samay);
	fprintf(file,"Raag description\n");
	fprintf(file,"-----------------\n");
	fprintf(file,"%s\n",r->raag_desc);
	fprintf(file,"-------------------------------------------------------------------\n");
	
	fprintf(file,"\n------Raag Introduction-----\n");
	fprintf(file,"Aaroh\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n\n",r->intro.aaroh);
	fprintf(file,"Avroh\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n\n",r->intro.avroh);
	fprintf(file,"Pakad\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n\n",r->intro.pakad);
	fprintf(file,"Swar-vistar\n");
	fprintf(file,"------------\n");
	fprintf(file,"%s\n\n",r->intro.swar_vistar);

	fprintf(file,"-------------------------------------------------------------------\n");
	fprintf(file,"\n\n-----Sargam-----\n");
	fprintf(file,"Sthai\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n",r->sargam.sthai);
	fprintf(file,"Antara\n");
	fprintf(file,"-------\n");
	fprintf(file,"%s\n",r->sargam.antara);
	fprintf(file,"-------------------------------------------------------------------\n");

	fprintf(file,"\n\n-----Gat-----\n");
	fprintf(file,"Sthai\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n",r->gat.sthai);
	fprintf(file,"Antara\n");
	fprintf(file,"-------\n");
	fprintf(file,"%s\n",r->gat.antara);

	fprintf(file,"-------------------------------------------------------------------\n");
	fprintf(file,"\n\n-----Aalap-----\n");
	fprintf(file,"Sthai\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n",r->aalap.sthai);
	fprintf(file,"Antara\n");
	fprintf(file,"-------\n");
	fprintf(file,"%s\n",r->aalap.antara);

	fprintf(file,"-------------------------------------------------------------------\n");
	fprintf(file,"\n\n-----Taana-----\n");
	fprintf(file,"Sthai\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n",r->taana.sthai);
	fprintf(file,"Antara\n");
	fprintf(file,"-------\n");
	fprintf(file,"%s\n",r->taana.antara);
	fprintf(file,"-------------------------------------------------------------------\n");
	fprintf(file,"***********************End**********************\n");

	fclose(file); //close the file after use
}

/* save: saves the raag r to the file. This is the bare basic raag export. 
 * This is not how end user expectes how the output should be.
 * For user, writeToFile() function is written */
void save(struct raag *r, char *path)
{
	//local variables
	FILE *file;
	
	//code
	/* Write contents to file */
	file = fopen(path, "w+"); //open the file in write mode, create if not present
	if(file == NULL)
	{
		printf("%s path not found\n",path);
		quit("save", 1);
	}
	//write to the file
	fprintf(file,"%s:",r->name); //1 - name
	fprintf(file,"%s:",r->thaat); //2 - thaat
	fprintf(file,"%s:",r->warjya_swar); //3 - warjya swar
	fprintf(file,"%s:",r->jati); //4 - jati
	fprintf(file,"%s:",r->vadi_swar); //5 - vadi swar
	fprintf(file,"%s:",r->samvadi_swar); //6 - samvadi swar
	fprintf(file,"%s:",r->swar_samay); //7 - samay
	fprintf(file,"%s:",r->raag_desc); //8 - desciption
	
	fprintf(file,"%s:",r->intro.aaroh); //9 - aaroh
	fprintf(file,"%s:",r->intro.avroh); //10 - avroh
	fprintf(file,"%s:",r->intro.pakad); // 11 - pakad
	fprintf(file,"%s:",r->intro.swar_vistar); //12 -swar vistar

	fprintf(file,"%s:",r->sargam.sthai); //13 - sargam sthai
	fprintf(file,"%s:",r->sargam.antara); //14 - sargam antara

	fprintf(file,"%s:",r->gat.sthai); //15 - gat sthai
	fprintf(file,"%s:",r->gat.antara); //16 - gat antara

	fprintf(file,"%s:",r->aalap.sthai); //17 - aalap sthai
	fprintf(file,"%s:",r->aalap.antara); //18 - aalap antara

	fprintf(file,"%s:",r->taana.sthai); //19 - taana sthai
	fprintf(file,"%s",r->taana.antara); //20 - taana antara


	fclose(file); //close the file after use

}

/* import_raag: import the raag in struct raag r */
void import_raag(char *path, struct raag *r)
{
	//local variables
	char buf[MAXBUF]; //for data transfer from file to struct
	FILE *file;
	char *last_token;
	const char *delimeter_char = ":";
	int i=0;

	//code
	//r = rs_alloc(sizeof(struct raag));
	//open the file
	file = fopen(path, "r");
	//check for any errors
	if(file == NULL)
	{
		quit(path, 1);
	}
	printf("file path - %s\n", path);
	
	fseek(file, 0, SEEK_END);
	//calculate the size of the file
	long fsize = ftell(file);
	fseek(file, 0, SEEK_SET); //rewind
	char *string = malloc(fsize + 1);
	fsize = fread(string, 1, fsize, file);
	//read the file line by line
	//while( fgets(buf, MAXBUF, file) != NULL)
	//if(fsize > 0)
	{
		//fputs( buf, stdout);
		//get last token
		last_token = (char *) strtok( string, delimeter_char);
		i=i+1; //increament i

		//iterate through all the delimited strings in this line
		while(last_token != NULL)
		{
			//assign the extracted values to structure
			switch(i)
			{
				case 1: //name
				r->name = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->name, last_token); //copy buffer contents to struct
				break;
			}					
			last_token = strtok(NULL, delimeter_char);
			if(last_token!=NULL)
				i=i+1; //increament i per token
			switch(i)
			{
				case 2: //thaat
				r->thaat = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->thaat, last_token); //copy buffer contents to struct
				break;
				case 3: //warjya swar
				r->warjya_swar = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->warjya_swar, last_token); //copy buffer contents to struct
				break;
				case 4://jati
				r->jati = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->jati, last_token); //copy buffer contents to struct
				break;
				case 5: //vadi swar
				r->vadi_swar = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->vadi_swar, last_token); //copy buffer contents to struct
				break;
				case 6: //samvadi swar
				r->samvadi_swar = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->samvadi_swar, last_token); //copy buffer contents to struct
				break;
				case 7: //swar samay
				r->swar_samay = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->swar_samay, last_token); //copy buffer contents to struct
				break;
				case 8: //description
				r->raag_desc = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->raag_desc, last_token); //copy buffer contents to struct
				break;
				case 9: //aaroh
				r->intro.aaroh = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->intro.aaroh, last_token); //copy buffer contents to struct
				break;
				case 10: //avroh
				r->intro.avroh = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->intro.avroh, last_token); //copy buffer contents to struct
				break;
				case 11: //pakad
				r->intro.pakad = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name

					rs_strcpy(r->intro.pakad, last_token); //copy buffer contents to struct
				break;
				case 12: //swar vistar
				r->intro.swar_vistar = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->intro.swar_vistar, last_token); //copy buffer contents to struct
				break;
				case 13: //sargam - sthai
				r->sargam.sthai = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->sargam.sthai, last_token); //copy buffer contents to struct
				break;
				case 14: //sargam - antara
				r->sargam.antara = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->sargam.antara, last_token); //copy buffer contents to struct
				break;
				case 15: //gat - sthai
				r->gat.sthai = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->gat.sthai, last_token); //copy buffer contents to struct
				break;
				case 16: //gat - antara
				r->gat.antara = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->gat.antara, last_token); //copy buffer contents to struct
				break;

				case 17: //aalap - sthai
				r->aalap.sthai = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->aalap.sthai, last_token); //copy buffer contents to struct
				break;
				case 18: //aalap - antara
				r->aalap.antara = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->aalap.antara, last_token); //copy buffer contents to struct
				break;

				case 19: //taana - sthai
				r->taana.sthai = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->taana.sthai, last_token); //copy buffer contents to struct
				break;
				case 20: //taana - antara
				if(last_token != NULL)
				{
				r->taana.antara = (char *) rs_alloc(rs_strlen(last_token)+1); //allocate size of source to name
				rs_strcpy(r->taana.antara, last_token); //copy buffer contents to struct
				}
				break;

			}
		}
	}

	
	//close the file
	fclose(file);
	string[fsize] = 0;
}

/* export_raag: export raag as per the fd provided */
void export_raag(FILE *file, struct raag *r)
{
	//check if fd is valid or not
	if(file == NULL)
	{
		quit("FdError", 1);
	}

	//write to the file
	fprintf(file,"Name - %s\n",r->name);
	fprintf(file,"Thaat - %s\n",r->thaat);
	fprintf(file,"Warjya swar - %s\n",r->warjya_swar);
	fprintf(file,"Jati - %s\n",r->jati);
	fprintf(file,"Vadi swar - %s\n",r->vadi_swar);
	fprintf(file,"Samvadi swar - %s\n",r->samvadi_swar);
	fprintf(file,"Swar samay - %s\n",r->swar_samay);
	fprintf(file,"Raag description\n");
	fprintf(file,"-----------------\n");
	fprintf(file,"%s\n",r->raag_desc);
	
	fprintf(file,"\n------Raag Introduction-----\n");
	fprintf(file,"Aaroh\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n\n",r->intro.aaroh);
	fprintf(file,"Avroh\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n\n",r->intro.avroh);
	fprintf(file,"Pakad\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n\n",r->intro.pakad);
	fprintf(file,"Swar-vistar\n");
	fprintf(file,"------------\n");
	fprintf(file,"%s\n\n",r->intro.swar_vistar);

	fprintf(file,"\n\n-----Sargam-----\n");
	fprintf(file,"Sthai\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n",r->sargam.sthai);
	fprintf(file,"Antara\n");
	fprintf(file,"-------\n");
	fprintf(file,"%s\n",r->sargam.antara);

	fprintf(file,"\n\n-----Gat-----\n");
	fprintf(file,"Sthai\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n",r->gat.sthai);
	fprintf(file,"Antara\n");
	fprintf(file,"-------\n");
	fprintf(file,"%s\n",r->gat.antara);

	fprintf(file,"\n\n-----Aalap-----\n");
	fprintf(file,"Sthai\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n",r->aalap.sthai);
	fprintf(file,"Antara\n");
	fprintf(file,"-------\n");
	fprintf(file,"%s\n",r->aalap.antara);

	fprintf(file,"\n\n-----Taana-----\n");
	fprintf(file,"Sthai\n");
	fprintf(file,"------\n");
	fprintf(file,"%s\n",r->taana.sthai);
	fprintf(file,"Antara\n");
	fprintf(file,"-------\n");
	fprintf(file,"%s\n",r->taana.antara);

} 
