/* Program: raag-app -- This program takes data related to raagas in indian classical music and display it on screen or moves to file */

/* header files */
#include "swaras.h"
#include "quit.h"
#include "strutils.h"
#include "memutils.h"
#include "console.h"
#include "raag.h"
#include "fileutils.h"
#include<fcntl.h>

//globa variables
char *file_path; //relative file path of the raag file

/* function prototype declarations */
int rs_getline(char *); //gets the line from console
void rs_strcpy(char *target, char *source); //copy source string to target
char *rs_alloc(int n); //allocates n chars of free space
void rs_free(char *p); //frees up space pointer by p
int rs_strlen(char *str); //returns length of the string excluding '\0'
void rs_list_dir(char *name);

/* entry-point function */
int main(void)
{
	//local vairables
	int ch; //to take input from console
	int raag_no; //to select raag no. to view
	struct raag *r;

	//function prototype declarations
	struct raag *getRaagData(void);
	void printRaag(struct raag *r);
	void writeToFile(struct raag *r, char *name);

	//code
	//Show options to the User
	do
	{
		printf("\n-------------------------\n");
		printf("1. Create New Raag\n");
		printf("2. Show Available Raagas\n");
		printf("3. View Raag\n");
		printf("4. Export Raag\n");
		printf("5. View Exported Raagas\n");
		printf("6. Update Raag\n");
		printf("0. Exit from the app\n");
		printf("Choose anyone from the above\n");
		printf("-------------------------\n");
		ch = getch(); //get input from console

		switch(ch)
		{
			case '1':
				//get details about raag from console
				r = getRaagData(); 
				printf("\n\nPrinting raag %s\n",r->name);
				printRaag(r); //prints the raag created above on the screen
				

				//allocate size equal to fully qualified name of raag file
				file_path = (char *) rs_alloc(rs_strlen(RAAG_DATA_FOLDER_PATH) 
				+ rs_strlen(r->name) + rs_strlen(RAAG_DATA_FILE_EXT) + 1);
				//create relative path of raagas folder	
				rs_create_file_path(file_path, RAAG_DATA_FOLDER_PATH,
					r->name, RAAG_DATA_FILE_EXT);

				
				printf("\n\nWriting to file %s\n", file_path);
				save(r, file_path); //write the raag to the file
			break;

			case '2':
				printf("\n\nList of available raagas are as follows\n");
				rs_list_dir(RAAG_DATA_FOLDER_PATH); //lists the available raagas in the system
			break;

			case '3':
				printf("\n\nList of available raagas are as follows\n");
				rs_list_dir(RAAG_DATA_FOLDER_PATH); //list the available raagas in the system
				printf("Choose which raag to view\n");
				raag_no = getch(); //provide the option to choose from the listed above
				raag_no = raag_no - '0'; //convert char to number
				rs_view_file_contents(raag_no,0, 0); //inside fileutils.c
			break;

			case '4':
				printf("\n\nList of available raagas are as follows\n");
				rs_list_dir(RAAG_DATA_FOLDER_PATH); //list the available raagas in the system
				printf("Choose which raag to view\n");
				raag_no = getch(); //provide the option to choose from the listed above
				raag_no = raag_no - '0'; //convert char to number
				rs_view_file_contents(raag_no,0, 1); //inside fileutils.c

			break;
			case '5':
				printf("\n\nList of available raagas are as follows\n");
				rs_list_dir(RAAG_FOLDER_PATH); //list the available raagas in the system
				printf("Choose which raag to view\n");
				raag_no = getch(); //provide the option to choose from the listed above
				raag_no = raag_no - '0'; //convert char to number
				rs_view_file_contents(raag_no,1, 0); //inside fileutils.c

			break;

			//update raag
			case '6':
				printf("\n\nList of available raagas are as follows\n");
				rs_list_dir(RAAG_DATA_FOLDER_PATH); //list the available raagas in the system
				printf("Choose which raag to update\n");
				raag_no = getch(); //provide the option to choose from the listed above
				raag_no = raag_no - '0'; //convert char to number
				rs_view_file_contents(raag_no,2, 0); //inside fileutils.c


			break;

			case '0':
				printf("Exiting from the app\n");
				quit("Case0", 1);
			break;

			default:
				printf("Choose correct option\n");
			break;
		}
	}while(ch != '0');
}

