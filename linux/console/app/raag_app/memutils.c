/* header files */
#include "memutils.h"

/* rs_alloc: allocates space for n character */
char *rs_alloc(int n)
{
	if((allocbuf + ALLOCSIZE - allocp) >= n) //if free space available
	{
		allocp = allocp + n; //next free pointer
		return allocp - n; //old pointer
	}else //not enough space
	{
		return 0;
	}
} //returns pointer to n characters


/* rs_free: free storage pointer by p */
void rs_free(char *p)
{
	if(p >= allocbuf && p<allocbuf + ALLOCSIZE) //check if p is allocated between the allocbuf
	{
		allocp = p; //set free pointer to point to p
	}
}
