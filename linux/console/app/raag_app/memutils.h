#ifndef H_MEMUTILS
#define H_MEMUTILS

/* header files */
#include<stdio.h>

/* global variable/macros declarations */
/* allocate size dynamically */
#define ALLOCSIZE 10000 //size of avaliable space

static char allocbuf[ALLOCSIZE]; //storage for alloc
static char *allocp = allocbuf; //next free position in buffer

/* function prototype declarations */
char *rs_alloc(int n); //allocates n chars of free space
void rs_free(char *p); //frees up space pointer by p

#endif
