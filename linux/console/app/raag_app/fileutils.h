#ifndef H_FILEUTILS
#define H_FILEUTILS

/* header files*/
#include<stdio.h>
#include<dirent.h>
#include "quit.h"
#include<fcntl.h>

/* function prototype declarations */
void rs_list_dir(char *name);  //list the contents of the directory; ls implementation
void rs_view_file_contents(int fileno, int isViewExportedFile, int isExportToFille); //view file contents; cat implementation
void rs_create_file_path(char *result, char *folder, char *filename,char *ext); //create file name and store it in result
int rs_show_file_contents(char *path); //read the file contents and show it on the screen
#endif
