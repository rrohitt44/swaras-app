/* header files */
#include "strutils.h"

/* rs_strcpy: copy source string to target */
void rs_strcpy(char *target, char *source)
{
	while(*source != '\0') //loop till null character is not found
	{
		*target = *source; //assign source character to target
		source++; //point source to next character
		target++; //point target to next character
	}
	*target = '\0';
}


/* rs_strlen: returns the length of the string str excluding '\0' */
int rs_strlen(char *str)
{
	int len=0; //length of the string
	while(*str != '\0') //loop till null character is not found
	{
		len++;
		str++;
	}
	return len;
}

/* rs_strstr: attache t at the end of s */
void rs_strstr(char *res, char *s, char *t)
{
	//go to the end of source string
	while(*s != '\0')
	{
		*res = *s;
		res++;
		s++;
	}

	//attache target to source
	while((*res = *t) != '\0')
	{
		*res = *t;
		res++;
		t++;
	}
	//*res='\0';
}
