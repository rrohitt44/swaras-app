#ifndef H_STRUTILS
#define H_STRUTILS
/* header files */
#include<stdio.h>

/* function prototype declarations */
void rs_strcpy(char *target, char *source); //copy source string to target
int rs_strlen(char *str); //returns length of the string excluding '\0'
void rs_strstr(char *result, char *s, char *t); //appends target to source string
#endif
